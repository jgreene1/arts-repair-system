﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerRepairs.aspx.cs" Inherits="WebApplication1.CustomerRepairs" %>

 <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <h2><%: Title %> My Repairs </h2>
     <p>       
     
     </p>
       <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketID,CustomerID1,UserAccountID" DataSourceID="SqlDataSource1">
         <Columns>
             <asp:HyperlinkField HeaderText="Ask a question about your repair" DataTextField="TicketNumber" DataNavigateUrlFields="TicketNumber" DataNavigateUrlFormatString="/RepairContact/?TicketNumber={0}" />
             <asp:BoundField DataField="TicketNumber" HeaderText="TicketNumber" SortExpression="TicketNumber" />
             <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
             <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make" />
             <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
             <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
             <asp:BoundField DataField="Serial" HeaderText="Serial" SortExpression="Serial" />
             <asp:BoundField DataField="DatePromised" HeaderText="DatePromised" SortExpression="DatePromised" />
             <asp:BoundField DataField="Accessories" HeaderText="Accessories" SortExpression="Accessories" />
             <asp:BoundField DataField="Problem" HeaderText="Problem" SortExpression="Problem" />
             <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
             <asp:BoundField DataField="Shop" HeaderText="Shop" SortExpression="Shop" />
             <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
         </Columns>
     </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="select * from repairs r join customers c ON r.customerID = c.customerID join useraccounts u on c.UserID = u.UserAccountID where username = @username">
          <SelectParameters>
              <asp:Parameter Name="username" Type="String"  />              
          </SelectParameters>
     </asp:SqlDataSource>
         </asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Models;

namespace WebApplication1
{
    public partial class NewRepair : System.Web.UI.Page
    {

        //sets the default view to insert
        protected void Page_Load(object sender, EventArgs e)
        {
            FormView1.DefaultMode = FormViewMode.Insert;
            //checks the view mode
            String mode = FormView1.CurrentMode.ToString();
          
            //if the view is not insert, sets it to the insert so that you see the new repair page.
                if (mode != "Insert")
                {
                    FormView1.ChangeMode(FormViewMode.Insert);
                }
        }
       
    }
}
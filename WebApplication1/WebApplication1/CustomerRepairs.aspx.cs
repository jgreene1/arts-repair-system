﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class CustomerRepairs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //gets the user name and sets the default value for the username paramter on the select command
            SqlDataSource1.SelectParameters["username"].DefaultValue = User.Identity.Name;
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //gets the user name and sets the default value for the username paramter on the select command
            SqlDataSource1.SelectParameters["username"].DefaultValue = User.Identity.Name;                     
        }
    }
}
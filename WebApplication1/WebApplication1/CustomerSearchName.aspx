﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeBehind="CustomerSearchName.aspx.cs" Inherits="WebApplication1.CustomerSearchName" %>

   
            
   <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
 <h2><%: Title %> Search For A Customer by Name</h2>
       
     <br />
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT * FROM [Customers] WHERE (([FirstName] Like '%' +@FirstName+ '%') OR ([LastName] Like '%' +@LastName+ '%'))">
         <SelectParameters>
             <asp:ControlParameter ControlID="FirstNameBox" Name="FirstName" DefaultValue=" " PropertyName="Text" Type="String" />
             <asp:ControlParameter ControlID="LastNameBox" Name="LastName" DefaultValue=" " PropertyName="Text" Type="String" />
         </SelectParameters>
     </asp:SqlDataSource>
     <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label><asp:TextBox ID="FirstNameBox" runat="server" ></asp:TextBox>
     <br />
     <asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label><asp:TextBox ID="LastNameBox" runat="server"></asp:TextBox>
    <br />
           <asp:LinkButton ID="SearchButton" runat="server" >Search Customers</asp:LinkButton>
     
       <br />
       <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CustomerID" DataSourceID="SqlDataSource1" AllowSorting="True" BorderColor="White" >
           <Columns>
               <asp:HyperlinkField HeaderText="Create Ticket" DataTextField="CustomerID" DataNavigateUrlFields="CustomerID" DataNavigateUrlFormatString="/NewRepair/?CustomerID={0}" />
               <asp:BoundField DataField="CustomerID" HeaderText=" Customer ID " SortExpression="CustomerID" InsertVisible="False" ReadOnly="True" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="FirstName" HeaderText=" First Name " SortExpression="FirstName" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" >               
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="SchoolName" HeaderText="School Name" SortExpression="SchoolName" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField  DataField="State" HeaderText="State" SortExpression="State" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
               <asp:BoundField DataField="ZipCode" HeaderText="Zip Code" SortExpression="ZipCode" >
               <ItemStyle BorderColor="White" BorderStyle="Solid" HorizontalAlign="Center" />
               </asp:BoundField>
           </Columns>
       </asp:GridView>
     
     <br />
          
</asp:Content>


    
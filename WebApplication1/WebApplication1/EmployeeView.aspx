﻿<%@ Page Title="Employee Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeView.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Art's Music Repair</h1>
        <p class="lead">Meeting all your instrument repair needs!</p>
        <p><a href="http://artsrepairproject.com/EmployeeRepairs" class="btn btn-default btn-lg">Your Repairs &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Search Customers</h2>       
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/CustomerSearchName">Search Customers &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Search Repairs</h2>          
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/FindRepair">Search Repairs &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Create a Repair</h2>
           
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/CreateRepair">New Repair &raquo;</a>
            </p>
        </div>

        <div class="col-md-4">
            <h2>View Completed Repairs</h2>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/ViewCompletedRepairs">View Repairs &raquo;</a>
            </p>
        </div>
 

      <div class="col-md-4">
            <h2>View Open Repairs</h2>           
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/OpenRepairs">Open Repairs &raquo;</a>
            </p>
        </div>
    

      <div class="col-md-4">
            <h2>Unapproved Repairs</h2>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/UnapprovedRepairs">Unapproved Repairs &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication1
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        System.Data.SqlClient.SqlConnection roleConn;
        System.Data.SqlClient.SqlCommand roleCmd;
        String roleQuerystr;
        String userRole;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AppendHeader("X-UA-Compatible", "IE=8");
            //This will check the users role and set the href for the navigation to the correct home page and update
            userRole = GetUserRole(Context.User.Identity.Name);
            if (userRole == "Employee") 
            {
                homeLink.Attributes["href"] = "/EmployeeView";
            }
            else if (userRole == "Customer")
            {
                homeLink.Attributes["href"] = "/CustomerView";
            }
            else if (userRole == "Client")
            {
                homeLink.Attributes["href"] = "/ClientView";
            }
            else if (userRole == "Admin")
            {
                homeLink.Attributes["href"] = "/AdminView";
            }
            else
            {
                homeLink.Attributes["href"] = "~/";
            }

            
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
        }

        protected string GetUserRole(string Username)
        {
            String userRole;
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["artsRepairsConnectionString"].ToString();
            roleConn = new System.Data.SqlClient.SqlConnection(connString);
            roleQuerystr = "";
            roleQuerystr = "SELECT Role " + "FROM UserAccounts " + "WHERE Username = @Username";
            roleCmd = new System.Data.SqlClient.SqlCommand(roleQuerystr, roleConn);
            SqlDataReader reader = null;
            roleCmd.Parameters.AddWithValue("@Username", Username);
            try
            {
                roleConn.Open();
                reader = roleCmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    userRole = reader["Role"].ToString();
                }
                else
                {
                    userRole = null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (roleConn != null)
                    roleConn.Close();
                if (reader != null)
                    reader.Close();
            }
            return userRole;
        }
        
    }

}
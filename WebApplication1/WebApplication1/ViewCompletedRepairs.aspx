﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewCompletedRepairs.aspx.cs" Inherits="WebApplication1.ViewCompletedRepairs" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> View Completed Customer Repairs </h2>
    <br />
       <asp:Label ID="Label1" runat="server" Text="First Name:"></asp:Label>
     <asp:TextBox ID="FirstNameBox" runat="server"></asp:TextBox>
     <br />
    <asp:Label ID="Label2" runat="server" Text="Last Name:"></asp:Label>
    <asp:TextBox ID="LastNameBox" runat="server"></asp:TextBox>
     <br />
     <asp:LinkButton ID="SearchButton" runat="server">View Customers</asp:LinkButton>
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT c.FirstName, c.LastName, TicketNumber, DateCreated, Make, Type, Model, Serial, DatePromised,
Accessories, Problem, Description, Shop, Total, DateClosed FROM [Repairs] r
JOIN Customers c ON
c.CustomerID = r.CustomerID
WHERE (([FirstName] Like '%' +@FirstName+ '%') OR ([LastName] Like '%' +@LastName+ '%'))
AND DateClosed IS NOT NULL">
         <SelectParameters>
             <asp:ControlParameter ControlID="FirstNameBox" Name="FirstName" DefaultValue=" " PropertyName="Text" Type="String" />
             <asp:ControlParameter ControlID="LastNameBox" Name="LastName" DefaultValue=" " PropertyName="Text" Type="String" />
         </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="TicketNumber" HeaderText="TicketNumber" SortExpression="TicketNumber" />
            <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
            <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make" />
            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
            <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
            <asp:BoundField DataField="Serial" HeaderText="Serial" SortExpression="Serial" />
            <asp:BoundField DataField="DatePromised" HeaderText="DatePromised" SortExpression="DatePromised" />
            <asp:BoundField DataField="Accessories" HeaderText="Accessories" SortExpression="Accessories" />
            <asp:BoundField DataField="Problem" HeaderText="Problem" SortExpression="Problem" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:BoundField DataField="Shop" HeaderText="Shop" SortExpression="Shop" />
            <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
            <asp:BoundField DataField="DateClosed" HeaderText="DateClosed" SortExpression="DateClosed" />
        </Columns>
    </asp:GridView>
     <br />
     <br />

</asp:Content>

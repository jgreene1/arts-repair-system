﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using WebApplication1.Models;

namespace WebApplication1.Account
{
    public partial class Register : Page
    {
        System.Data.SqlClient.SqlConnection conn;
        System.Data.SqlClient.SqlCommand cmd;
        System.Data.SqlClient.SqlCommand cmd1;
        String querystr;
        String querystr1;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            registerUser();
        }

        private void registerUser()
        {
            //checks the data for validity
            if (IsValid)
            {
                //asp generated user code, uses the asp database
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
                var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text };
                IdentityResult result = manager.Create(user, Password.Text);

                //adds a user to the arts database side for documentation purposes
              

                //if the code succedes it logs the user in and displays their username
                if (result.Succeeded)
                {  

                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["artsRepairsConnectionString"].ToString();
                conn = new System.Data.SqlClient.SqlConnection(connString);
                conn.Open();
                querystr = "";
                querystr1 = "";

                 querystr = "INSERT INTO UserAccounts (Username, Password, Role, Active)" +
                "VALUES('" + Email.Text + "','" + Password.Text + "','" + "Customer" + "','" + 1 + "')";
                cmd = new System.Data.SqlClient.SqlCommand(querystr, conn);
                cmd.ExecuteReader();
                String userQueryStr = "SELECT UserAccountID FROM UserAccounts WHERE Username =     '" + Email.Text + "'";
                conn.Close();
                conn.Open();
                System.Data.SqlClient.SqlCommand selectCommand = new System.Data.SqlClient.SqlCommand(userQueryStr, conn);
                int userID = Convert.ToInt32(selectCommand.ExecuteScalar());
                querystr1 = "INSERT INTO Customers (FirstName, LastName, StreetAddress1, Phone, SchoolName, City, State, ZipCode, UserID)" +
                    "VALUES ('" + FirstName.Text + "','" + LastName.Text + "','" + StreetAddress.Text + "','" + Phone.Text + "','" + School.Text + "','" + City.Text + "','" + State.Text + "','" + Zip.Text + "','" + userID + "')";

                cmd1 = new System.Data.SqlClient.SqlCommand(querystr1, conn);
                cmd1.ExecuteReader();
                conn.Close();
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    //string code = manager.GenerateEmailConfirmationToken(user.Id);
                    //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                    //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>."); 
              

                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                }
            }
           
        }
    }
}
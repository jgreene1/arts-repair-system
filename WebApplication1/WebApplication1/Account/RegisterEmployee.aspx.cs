﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using WebApplication1.Models;
using System.Web.Mvc;

namespace WebApplication1.Account
{
    public partial class Register : Page
    {
        System.Data.SqlClient.SqlConnection empConn;
        System.Data.SqlClient.SqlCommand empCmd;
        System.Data.SqlClient.SqlCommand empCmd1;
        String empQuerystr;
        String empQuerystr1;

        [Authorize]
        protected void EmpPage_Load(object sender, EventArgs e)
        {


        }
        /// <summary>
        /// This method executes when the user clicks the register button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        protected void CreateEmpUser_Click(object sender, EventArgs e)
        {
            registerEmployeeUser();
        }
        /// <summary>
        /// This is the method that will register the Employee to the UserAccounts table and Then Add them to the employees table using the userID assigned 
        /// </summary>
        private void registerEmployeeUser()
        {
            //checks the data for validity
            if (IsValid)
            {
                //asp generated user code, uses the asp database
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
                var user = new ApplicationUser() { UserName = EmpEmail.Text, Email = EmpEmail.Text };
                IdentityResult result = manager.Create(user, EmpPassword.Text);

                //adds a user to the arts database side for documentation purposes


                //if the code succeeds it logs the user in and displays their username
                if (result.Succeeded)
                {

                    String connString = System.Configuration.ConfigurationManager.ConnectionStrings["artsRepairsConnectionString"].ToString();
                    empConn = new System.Data.SqlClient.SqlConnection(connString);
                    empConn.Open();
                    empQuerystr = "";
                    empQuerystr1 = "";

                    empQuerystr = "INSERT INTO UserAccounts (Username, Password, Role, Active)" +
                        "VALUES('" + EmpEmail.Text + "','" + EmpPassword.Text + "','" + "Employee" + "','" + 1 + "')";
                    empCmd = new System.Data.SqlClient.SqlCommand(empQuerystr, empConn);
                    empCmd.ExecuteReader();
                    String userQueryStr = "SELECT UserAccountID FROM UserAccounts WHERE Username =     '" + EmpEmail.Text + "'";
                    empConn.Close();
                    empConn.Open();
                    System.Data.SqlClient.SqlCommand selectCommand = new System.Data.SqlClient.SqlCommand(userQueryStr, empConn);
                    int userID = Convert.ToInt32(selectCommand.ExecuteScalar());
                    empQuerystr1 = "INSERT INTO Employees (FirstName, Minit, LastName, Store, UserID)" +
                        "VALUES ('" + EmpFirstName.Text + "','" + EmpMinit.Text + "','" + EmpLastName.Text + "','" + EmpStore.Text + "','" + userID + "')";

                    empCmd1 = new System.Data.SqlClient.SqlCommand(empQuerystr1, empConn);
                    empCmd1.ExecuteReader();
                    empConn.Close();

                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }
                else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                }
            }


        }

       
    }
}
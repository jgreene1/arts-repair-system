﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using WebApplication1.Models;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;


namespace WebApplication1.Account
{
    
    public partial class Login : Page
    {
        System.Data.SqlClient.SqlConnection roleConn;
        System.Data.SqlClient.SqlCommand roleCmd;
        String roleQuerystr;
        String userRole;

          
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        /// <summary>
        /// This method should get the user's role from the Art's DB
        /// </summary>
        /// <param name="Username"></param>
        /// <returns></returns>
        protected string GetUserRole(string Username)
        {
            String userRole;
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["artsRepairsConnectionString"].ToString();
            roleConn = new System.Data.SqlClient.SqlConnection(connString);
            roleQuerystr = "";
            roleQuerystr = "SELECT Role " + "FROM UserAccounts " + "WHERE Username = @Username";
            roleCmd = new System.Data.SqlClient.SqlCommand(roleQuerystr, roleConn);
            SqlDataReader reader = null;
            roleCmd.Parameters.AddWithValue("@Username", Username);
            try
            {
                roleConn.Open();
                reader = roleCmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    userRole = reader["Role"].ToString();
                }
                else
                {
                    userRole = null;
                }
            }
            catch (SqlException ex) 
            {
                throw ex;
            }
            finally
            {
                if (roleConn != null)
                    roleConn.Close();
                if (reader != null)
                    reader.Close();
            }
            return userRole;
        }
        
        /// <summary>
        /// This method will LogIn the user with both the webapp db and artsmusic db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void LogIn(object sender, EventArgs e)
        {
           // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                switch (result)
                {
                    //On sucess this if statement will use the getUserRole method to check the role of the current user and redirect to proper page
                    case SignInStatus.Success:
                        userRole = GetUserRole(Email.Text);
                        if (userRole == "Employee")
                        {
                            Response.Redirect("/EmployeeView");
                        }
                        else if (userRole == "Customer")
                        {
                            Response.Redirect("/CustomerView");
                        }
                        else if (userRole == "Client")
                        {
                            Response.Redirect("/ClientView");
                        }
                        else if (userRole == "Admin")
                        {
                            Response.Redirect("/AdminView");
                        }
                        else
                        {
                            Response.Redirect("/Default");
                        }
                       
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }

            }
        }
   }
    
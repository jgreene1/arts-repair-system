﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication1.Account
{
    public partial class UpdateCustomerRegistration : System.Web.UI.Page
    {
        System.Data.SqlClient.SqlConnection roleConn;
        System.Data.SqlClient.SqlCommand roleCmd;
        String roleQuerystr;
        String userRole;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            FormView1.DefaultMode = FormViewMode.Edit;
            FormView2.DefaultMode = FormViewMode.Edit;
            UserNameLabel1.Visible = false;
            UserNameLabel1.Text = Context.User.Identity.Name;
            userRole = GetUserRole(Context.User.Identity.Name);

            if (userRole == "Customer")
            {
                FormView2.Visible = true;

            }

           else if (userRole == "Employee")
            {
                FormView1.Visible = true;

            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            String mode = FormView1.CurrentMode.ToString();
            String mode1 = FormView2.CurrentMode.ToString();

            if (mode != "Edit")
            {
                FormView1.ChangeMode(FormViewMode.Edit);
            }

            if (mode1 != "Edit")
            {
                FormView2.ChangeMode(FormViewMode.Edit);
            }

            if (Page.IsValid)
            {
                FormView1.UpdateItem(true);
                FormView2.UpdateItem(true);
            }
        }

        protected string GetUserRole(string Username)
        {
            String userRole;
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["artsRepairsConnectionString"].ToString();
            roleConn = new System.Data.SqlClient.SqlConnection(connString);
            roleQuerystr = "";
            roleQuerystr = "SELECT Role " + "FROM UserAccounts " + "WHERE Username = @Username";
            roleCmd = new System.Data.SqlClient.SqlCommand(roleQuerystr, roleConn);
            SqlDataReader reader = null;
            roleCmd.Parameters.AddWithValue("@Username", Username);
            try
            {
                roleConn.Open();
                reader = roleCmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    userRole = reader["Role"].ToString();
                }
                else
                {
                    userRole = null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (roleConn != null)
                    roleConn.Close();
                if (reader != null)
                    reader.Close();
            }
            return userRole;
        }
    }
}
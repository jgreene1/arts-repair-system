﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterEmployee.aspx.cs" Inherits="WebApplication1.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="EmpErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmpEmail" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="EmpEmail" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpEmail"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmpPassword" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="EmpPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpPassword"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmpConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="EmpConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="EmpPassword" ControlToValidate="EmpConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmpFirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
            <div class="col-md-offset-2 col-md-10">
                <asp:TextBox runat="server" ID="EmpFirstName" TextMode="SingleLine" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpFirstName" CssClass="text-danger" ErrorMessage="The first name field is required"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
             <asp:Label runat="server" AssociatedControlID="EmpMinit" CssClass="col-md-2 control-label">Middle Initial</asp:Label>
            <div class="col-md-offset-2 col-md-10">
                 <asp:TextBox runat="server" ID="EmpMinit" TextMode="SingleLine" CssClass="form-control" />
                 <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpMinit" CssClass="text-danger" ErrorMessage="Middle Initial field is required"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmpLastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
            <div class="col-md-offset-2 col-md-10">
                 <asp:TextBox runat="server" ID="EmpLastName" CssClass="form-control" />
                 <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpLastName" CssClass="text-danger" ErrorMessage="Last Name field is required"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">          
            <asp:Label runat="server" AssociatedControlID="EmpStore" CssClass="col-md-2 control-label">Store</asp:Label>
            <div class="col-md-offset-2 col-md-10">
                <asp:TextBox runat="server" ID="EmpStore" TextMode="SingleLine" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmpStore" CssClass="text-danger" ErrorMessage="Phone field is required"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                
                <asp:Button runat="server" OnClick="CreateEmpUser_Click" Text="Register" CssClass="btn btn-default" ID="EmpButton1" />
                
                
            </div>
        </div>
    </div>
</asp:Content>

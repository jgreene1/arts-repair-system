﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateRegistration.aspx.cs" Inherits="WebApplication1.Account.UpdateCustomerRegistration" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <p>
        <asp:Label ID="UserNameLabel1" runat="server" Text="UserName:"></asp:Label>
    </p>
    <p>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1">
            <EditItemTemplate>
                <h2><%: Title %> Update Employee Information</h2>
                FirstName:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                Store:
                <asp:TextBox ID="StoreTextBox" runat="server" Text='<%# Bind("Store") %>' />
                <br />
                UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                FirstName:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                Store:
                <asp:TextBox ID="StoreTextBox" runat="server" Text='<%# Bind("Store") %>' />
                <br />
                UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                FirstName:
                <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                Store:
                <asp:Label ID="StoreLabel" runat="server" Text='<%# Bind("Store") %>' />
                <br />

                UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            </ItemTemplate>
        </asp:FormView>
    </p>
    <p>
        <asp:FormView ID="FormView2" runat="server" DataSourceID="SqlDataSource2">
            
            <EditItemTemplate>
                <h2><%: Title %> Update Customer Information</h2>
                FirstName:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                StreetAddress1:
                <asp:TextBox ID="StreetAddress1TextBox" runat="server" Text='<%# Bind("StreetAddress1") %>' />
                <br />
                Phone:
                <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                <br />
                SchoolName:
                <asp:TextBox ID="SchoolNameTextBox" runat="server" Text='<%# Bind("SchoolName") %>' />
                <br />
                City:
                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                <br />
                State:
                <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
                <br />
                ZipCode:
                <asp:TextBox ID="ZipCodeTextBox" runat="server" Text='<%# Bind("ZipCode") %>' />
                <br />
                UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                <h2><%: Title %> Update Employee Information</h2>
                FirstName:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                StreetAddress1:
                <asp:TextBox ID="StreetAddress1TextBox" runat="server" Text='<%# Bind("StreetAddress1") %>' />
                <br />
                Phone:
                <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                <br />
                SchoolName:
                <asp:TextBox ID="SchoolNameTextBox" runat="server" Text='<%# Bind("SchoolName") %>' />
                <br />
                City:
                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                <br />
                State:
                <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
                <br />
                ZipCode:
                <asp:TextBox ID="ZipCodeTextBox" runat="server" Text='<%# Bind("ZipCode") %>' />
                <br />
                 UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                FirstName:
                <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' />
                <br />
                LastName:
                <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' />
                <br />
                StreetAddress1:
                <asp:Label ID="StreetAddress1Label" runat="server" Text='<%# Bind("StreetAddress1") %>' />
                <br />
                Phone:
                <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
                <br />
                SchoolName:
                <asp:Label ID="SchoolNameLabel" runat="server" Text='<%# Bind("SchoolName") %>' />
                <br />
                City:
                <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>' />
                <br />
                State:
                <asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>' />
                <br />
                ZipCode:
                <asp:Label ID="ZipCodeLabel" runat="server" Text='<%# Bind("ZipCode") %>' />
                <br />
                UserID:
                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Bind("UserID") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            </ItemTemplate>
        </asp:FormView>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT FirstName, LastName, Store, e.UserID
FROM Employees e
INNER JOIN UserAccounts u ON
e.UserID = u.UserAccountID
WHERE UserName = @UserName
AND Role = 'Employee'" UpdateCommand="UPDATE Employees
SET FirstName = @FirstName, LastName = @LastName, Store = @Store
WHERE UserID = @UserID">
            <SelectParameters>
                <asp:ControlParameter ControlID="UserNameLabel1" Name="UserName" PropertyName="Text" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="FirstName" />
                <asp:Parameter Name="LastName" />
                <asp:Parameter Name="Store" />
                <asp:Parameter Name="UserID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT FirstName, LastName, StreetAddress1, Phone, SchoolName, City, State, ZipCode, UserID
FROM Customers
INNER JOIN UserAccounts
ON Customers.UserID = UserAccounts.UserAccountID
WHERE Username = @Username
AND Role = 'Customer'" UpdateCommand="UPDATE Customers SET FirstName = @FirstName, LastName = @LastName, StreetAddress1 = @StreetAddress1, Phone = @Phone, SchoolName = @SchoolName, City = @City, State = @State, ZipCode = @ZipCode
FROM Customers

WHERE UserID = @UserID">
            <SelectParameters>
                <asp:ControlParameter ControlID="UserNameLabel1" Name="Username" PropertyName="Text" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="FirstName" />
                <asp:Parameter Name="LastName" />
                <asp:Parameter Name="StreetAddress1" />
                <asp:Parameter Name="Phone" />
                <asp:Parameter Name="SchoolName" />
                <asp:Parameter Name="City" />
                <asp:Parameter Name="State" />
                <asp:Parameter Name="ZipCode" />
                <asp:Parameter Name="UserID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
</asp:Content>

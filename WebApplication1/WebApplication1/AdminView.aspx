﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminView.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Art's Music Repair</h1>
        <p class="lead">Meeting all your instrument repair needs!</p>
        <p><a href="http://artsrepairproject.com/About" class="btn btn-default btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Get your repair status</h2>
            <p>
                Click here to search for the status of your repair.
            </p>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/Repair">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Register Employee</h2>
            <p>
               Click here to register a new employee
            </p>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/Account/RegisterEmployee">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Update Registration</h2>
            <p>
                Click here to register a new Client
            </p>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/Account/UpdateRegistration">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>

﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RepairContact.aspx.cs" Inherits="WebApplication1.RepairContact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Have a question for one of our stores or need information? Fill out the form below, and one of our service representatives will contact you. </h3>
  <h6>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" >            
      </asp:SqlDataSource>
      <br />
                <asp:Label ID="Label1" runat="server" Text="Please Direct my Inquirey to"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" BackColor="SlateGray">
                    <asp:ListItem>Montgomery</asp:ListItem>
                    <asp:ListItem>Dothan</asp:ListItem>
                    <asp:ListItem>Birmingham</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:Label ID="Label6" runat="server" Text='<%# Request.QueryString.Get("TicketNumber") %>' ></asp:Label>              
                <br />
                <br />
                <asp:Label ID="Label2" runat="server" Text="First Name" ></asp:Label>
                <asp:TextBox ID="TextBox2" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Last Name "></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="Email"></asp:Label>
                <asp:TextBox ID="TextBox4" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label5" runat="server" Text="Comment"></asp:Label>              
                <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                 <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
                <br />
          </h6>
      <br />
      
    
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeBehind="RepRepairs.aspx.cs" Inherits="WebApplication1.RepRepair" %>

   
            
   <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       <h2><%: Title %> Find Repairs for an Employee</h2>
     <br />
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT * FROM [Repairs] WHERE ([RepID] = @RepID)">
         <SelectParameters>
             <asp:ControlParameter ControlID="RepIDBox" Name="RepID" PropertyName="Text" Type="String" />            
         </SelectParameters>
     </asp:SqlDataSource>
     <asp:Label ID="Label1" runat="server" Text="Rep ID"></asp:Label><asp:TextBox ID="RepIDBox" runat="server"></asp:TextBox>
     <br />    
           <asp:LinkButton ID="SearchButton" runat="server" >Search Repairs</asp:LinkButton>
     
       <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketID" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True">
           <Columns>
             <asp:HyperlinkField HeaderText="Edit Repair" DataTextField="TicketNumber" DataNavigateUrlFields="TicketNumber" DataNavigateUrlFormatString="/EditRepair/?TicketNumber={0}" />
               <asp:BoundField DataField="TicketID" HeaderText="TicketID" InsertVisible="False" ReadOnly="True" SortExpression="TicketID" />
               <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" />
               <asp:BoundField DataField="RepID" HeaderText="RepID" SortExpression="RepID" />
               <asp:BoundField DataField="TechID" HeaderText="TechID" SortExpression="TechID" />
               <asp:BoundField DataField="TicketNumber" HeaderText="TicketNumber" SortExpression="TicketNumber" />
               <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
               <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make" />
               <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
               <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
               <asp:BoundField DataField="Serial" HeaderText="Serial" SortExpression="Serial" />
               <asp:BoundField DataField="DatePromised" HeaderText="DatePromised" SortExpression="DatePromised" />
               <asp:BoundField DataField="Accessories" HeaderText="Accessories" SortExpression="Accessories" />
               <asp:BoundField DataField="Problem" HeaderText="Problem" SortExpression="Problem" />
               <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
               <asp:BoundField DataField="Shop" HeaderText="Shop" SortExpression="Shop" />
               <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
           </Columns>
       </asp:GridView>
     
     <br />
          
</asp:Content>


    
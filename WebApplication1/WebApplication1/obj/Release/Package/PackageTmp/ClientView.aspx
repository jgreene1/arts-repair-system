﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientView.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Art's Music Repair</h1>
        <p class="lead">Meeting all your instrument repair needs!</p>
        <p><a href="http://artsrepairproject.com/About" class="btn btn-default btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Get your repair status</h2>
            <p>
                Click here to search for the status of your repair.
            </p>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/CustomerRepairs">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Submit Inquiery</h2>
            <p>
               Click here to submit and inquiry about your instrument.
            </p>
            <p>
                <a class="btn btn-default" href="http://artsrepairproject.com/Contact">Learn more &raquo;</a>
            </p>
        </div>
      
    </div>

</asp:Content>

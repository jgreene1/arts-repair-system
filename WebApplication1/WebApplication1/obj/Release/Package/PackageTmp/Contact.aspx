﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebApplication1.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Have a question for one of our stores or need information? Fill out the form below, and one of our service representatives will contact you. </h3>
  <h6>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ArtsRepairsConnectionString2 %>"
          InsertCommand="INSERT INTO Repairs(CustomerID, RepID, TechID, TicketNumber, DateCreated, Make, Type, Model, DatePromised, Serial, Accessories, Problem, Shop, Description, Total) VALUES ([CustomerID], [RepID], [TechID], [TicketNumber], GetDate(), [Make], [Type], [Model], [DatePromised], [Serial], [Accessories], [Problem], [Shop], [Description],  [Total])"
          SelectCommand="SELECT [CustomerID], [RepID], [TechID], [TicketNumber], [DateCreated], [Make], [Type], [Total], [Shop], [Description], [Problem], [Accessories], [DatePromised], [Model] FROM [Repairs]">
          <InsertParameters>
              <asp:Parameter Name="CustomerID" Type="Int32" />
              <asp:Parameter Name="RepID" Type="Int32" />
              <asp:Parameter Name="TechID" Type="Int32" />
              <asp:Parameter Name="TicketNumber" Type="String" />
              <asp:Parameter Name="Make" Type="String" />
              <asp:Parameter Name="Type" Type="String" />
              <asp:Parameter Name="Model" Type="String" />
              <asp:Parameter Name="DatePromised" Type="DateTime" />
              <asp:Parameter Name="Serial" Type="Int32" />
              <asp:Parameter Name="Accessories" Type="Int32" />
              <asp:Parameter Name="Problem" Type="String" />
              <asp:Parameter Name="Shop" Type="String" />
              <asp:Parameter Name="Description" Type="String" />
              <asp:Parameter Name="Total" Type="Decimal" />
          </InsertParameters>
      </asp:SqlDataSource>
                <asp:Label ID="Label1" runat="server" Text="Please Direct my Inquirey to"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" BackColor="SlateGray">
                    <asp:ListItem>Montgomery</asp:ListItem>
                    <asp:ListItem>Dothan</asp:ListItem>
                    <asp:ListItem>Birmingham</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="Label2" runat="server" Text="First Name" ></asp:Label>
                <asp:TextBox ID="TextBox2" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Last Name "></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="Email"></asp:Label>
                <asp:TextBox ID="TextBox4" runat="server" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label5" runat="server" Text="Comment"></asp:Label>
                Which
                <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" BackColor="SlateGray"></asp:TextBox>
                <br />
                <br />
                 <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
                <br />
          </h6>
      <br />
      
    
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditRepair.aspx.cs" Inherits="WebApplication1.EditRepair" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %> Update a Repair Ticket  </h2>
            
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="TicketID" DataSourceID="SqlDataSource1">
        <EditItemTemplate>
           TicketNumber:
            <asp:Label ID="TicketNumberTextBox" runat="server" Text='<%# Bind("TicketNumber") %>' />
            <br />
            DateCreated:
            <asp:Label ID="DateCreatedTextBox" runat="server" Text='<%# Bind("DateCreated") %>' />
            <br />
            CustomerID:
            <asp:TextBox ID="CustomerIDTextBox" runat="server" Text='<%# Bind("CustomerID") %>' />
            <br />
            RepID:
            <asp:TextBox ID="RepIDTextBox" runat="server" Text='<%# Bind("RepID") %>' />
            <br />
            TechID:
            <asp:TextBox ID="TechIDTextBox" runat="server" Text='<%# Bind("TechID") %>' />
            <br />
            Make:
            <asp:TextBox ID="MakeTextBox" runat="server" Text='<%# Bind("Make") %>' />
            <br />
            Type:
            <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>' />
            <br />
            Model:
            <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
            <br />
            Serial:
            <asp:TextBox ID="SerialTextBox" runat="server" Text='<%# Bind("Serial") %>' />
            <br />
            DatePromised:
            <asp:Label ID="DatePromisedLabel" runat="server" Text='<%# Bind("DatePromised") %>' /> 
            <br />
            Accessories:
            <asp:TextBox ID="AccessoriesTextBox" runat="server" Text='<%# Bind("Accessories") %>' />
            <br />
            Problem:
            <asp:TextBox ID="ProblemTextBox" runat="server" Text='<%# Bind("Problem") %>' />
            <br />
            Description:
            <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
            <br />
            Shop:
            <asp:DropDownList ID="DropDownList1" runat="server" BackColor="SlateGray" Width="150px" SelectedValue='<%# Bind("Shop") %>' >
                    <asp:ListItem >Montgomery</asp:ListItem>
                    <asp:ListItem >Dothan</asp:ListItem>
                    <asp:ListItem >Birmingham</asp:ListItem>
                </asp:DropDownList>
            <br />
            Total:
            <asp:TextBox ID="TotalTextBox" runat="server" Text='<%# Bind("Total") %>' />
            <br />
            Close Date:
            <asp:TextBox ID="ClosedDateBox" runat="server" Text='<%# Bind("DateClosed") %>' />
            <asp:RegularExpressionValidator ControlToValidate="ClosedDateBox" runat="server" CssClass="text-danger" ErrorMessage="Date Format Incorrect" ValidationExpression="^(((((0[13578])|([13578])|(1[02]))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(3[01])))|((([469])|(11))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(30)))|((02|2)[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9]))))[\-\/\s]?\d{4})(\s(((0[1-9])|([1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$" />
            <br />
            
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" PostBackUrl="~/FindRepair.aspx" />
        </EditItemTemplate> 
        <ItemTemplate>
            TicketID:
            <asp:Label ID="TicketIDLabel" runat="server" Text='<%# Eval("TicketID") %>' />
            <br />
            CustomerID:
            <asp:Label ID="CustomerIDLabel" runat="server" Text='<%# Bind("CustomerID") %>' />
            <br />
            RepID:
            <asp:Label ID="RepIDLabel" runat="server" Text='<%# Bind("RepID") %>' />
            <br />
            TechID:
            <asp:Label ID="TechIDLabel" runat="server" Text='<%# Bind("TechID") %>' />
            <br />
            TicketNumber:
            <asp:Label ID="TicketNumberLabel" runat="server" Text='<%# Bind("TicketNumber") %>' />
            <br />
            DateCreated:
            <asp:Label ID="DateCreatedLabel" runat="server" Text='<%# Bind("DateCreated") %>' />
            <br />
            Make:
            <asp:Label ID="MakeLabel" runat="server" Text='<%# Bind("Make") %>' />
            <br />
            Type:
            <asp:Label ID="TypeLabel" runat="server" Text='<%# Bind("Type") %>' />
            <br />
            Model:
            <asp:Label ID="ModelLabel" runat="server" Text='<%# Bind("Model") %>' />
            <br />
            Serial:
            <asp:Label ID="SerialLabel" runat="server" Text='<%# Bind("Serial") %>' />
            <br />
            DatePromised:
            <asp:Label ID="DatePromisedLabel" runat="server" Text='<%# Bind("DatePromised") %>' />
            <br />
            Accessories:
            <asp:Label ID="AccessoriesLabel" runat="server" Text='<%# Bind("Accessories") %>' />
            <br />
            Problem:
            <asp:Label ID="ProblemLabel" runat="server" Text='<%# Bind("Problem") %>' />
            <br />
            Description:
            <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>' />
            <br />
            Shop:
            <asp:Label ID="ShopLabel" runat="server" Text='<%# Bind("Shop") %>' />
            <br />
            Total:
            <asp:Label ID="TotalLabel" runat="server" Text='<%# Bind("Total") %>' />
            <br />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="Select * FROM [Repairs] WHERE ([TicketNumber] = @TicketNumber)" UpdateCommand= "UPDATE Repairs SET CustomerID=@CustomerID, RepID=@RepID, TechID=@TechID, Make=@Make, [Type]=@Type, Model=@Model, DatePromised=@DatePromised, Serial=@Serial, Accessories=@Accessories, Problem=@Problem, Shop=@Shop, [Description]=@Description, Total=@Total, DateClosed=@DateClosed 
WHERE TicketNumber = @TicketNumber"   >
        <SelectParameters>
            <asp:QueryStringParameter Name="TicketNumber" QueryStringField="TicketNumber" Type="String" />        
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="CustomerID" />
            <asp:Parameter Name="RepID" />
            <asp:Parameter Name="TechID" />
            <asp:Parameter Name="Make" />
            <asp:Parameter Name="Type" />
            <asp:Parameter Name="Model" />
            <asp:Parameter Name="DatePromised" />
            <asp:Parameter Name="Serial" />
            <asp:Parameter Name="Accessories" />
            <asp:Parameter Name="Problem" />
            <asp:Parameter Name="Shop" />
            <asp:Parameter Name="Description" />
            <asp:Parameter Name="Total" />
            <asp:Parameter Name="DateClosed" />
            <asp:Parameter Name="TicketNumber" />
        </UpdateParameters>
    </asp:SqlDataSource>

</asp:Content>

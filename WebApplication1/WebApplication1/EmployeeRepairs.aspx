﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeRepairs.aspx.cs" Inherits="WebApplication1.ViewOpenRepairs" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> View Open Customer Repairs </h2>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="select TicketNumber, DateCreated, Make, [Type], Model, Serial, DatePromised, Accessories, Problem, [Description], Shop, Total, DateClosed from Repairs r 
JOIN Employees e ON r.TechID = e.EmployeeID 
JOIN UserAccounts u on e.UserID = u.UserAccountID 
WHERE DateClosed IS NULL
AND UserName = @UserName">
        <SelectParameters>
            <asp:Parameter Name="UserName" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="TicketNumber" HeaderText="TicketNumber" SortExpression="TicketNumber" />
            <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
            <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make" />
            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
            <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
            <asp:BoundField DataField="Serial" HeaderText="Serial" SortExpression="Serial" />
            <asp:BoundField DataField="DatePromised" HeaderText="DatePromised" SortExpression="DatePromised" />
            <asp:BoundField DataField="Accessories" HeaderText="Accessories" SortExpression="Accessories" />
            <asp:BoundField DataField="Problem" HeaderText="Problem" SortExpression="Problem" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:BoundField DataField="Shop" HeaderText="Shop" SortExpression="Shop" />
            <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
            <asp:BoundField DataField="DateClosed" HeaderText="DateClosed" SortExpression="DateClosed" />
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeBehind="UnapprovedRepairs.aspx.cs" Inherits="WebApplication1.UnapprovedRepairs" %>

   
            
   <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       <h2><%: Title %> Unapproved Repairs </h2>
     <br />
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT * FROM [Repairs] WHERE ([DateClosed] is NULL)">
        
     </asp:SqlDataSource>
     
       <asp:GridView ID="GridView1" runat="server" CellPadding="5" AutoGenerateColumns="False" DataKeyNames="TicketID" DataSourceID="SqlDataSource1" AllowPaging="True">
           <Columns>
             <asp:HyperlinkField HeaderText="Edit Repair" DataTextField="TicketNumber" DataNavigateUrlFields="TicketNumber" DataNavigateUrlFormatString="/EditRepair/?TicketNumber={0}" />
               <asp:BoundField DataField="TicketID" HeaderText="TicketID" InsertVisible="False" ReadOnly="True" SortExpression="TicketID" />
               <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" />
               <asp:BoundField DataField="RepID" HeaderText="RepID" SortExpression="RepID" />
               <asp:BoundField DataField="TechID" HeaderText="TechID" SortExpression="TechID" />
               <asp:BoundField DataField="TicketNumber" HeaderText="TicketNumber" SortExpression="TicketNumber" />
               <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" SortExpression="DateCreated" />
               <asp:BoundField DataField="Make" HeaderText="Make" SortExpression="Make" />
               <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
               <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
               <asp:BoundField DataField="Serial" HeaderText="Serial" SortExpression="Serial" />
               <asp:BoundField DataField="DatePromised" HeaderText="DatePromised" SortExpression="DatePromised" />
               <asp:BoundField DataField="Accessories" HeaderText="Accessories" SortExpression="Accessories" />
               <asp:BoundField DataField="Problem" HeaderText="Problem" SortExpression="Problem" />
               <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
               <asp:BoundField DataField="Shop" HeaderText="Shop" SortExpression="Shop" />
               <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
               <asp:BoundField DataField="DateClosed" HeaderText="Date Closed" SortExpression="DateClosed" />
           </Columns>
       </asp:GridView>
     
     <br />
          
</asp:Content>


    
﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Repair.aspx.cs" Inherits="WebApplication1.Repair" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> Repairs</h2>
   <p>Art’s Music Shop provides in-store repair for the following <strong>brass instruments</strong>:</p>
<ul>
<li>Trumpets</li>
<li>Cornets</li>
<li>Bugles</li>
<li>Flugelhorns</li>
<li>French Horns</li>
<li>Mellophones</li>
<li>Baritones</li>
<li>Euphoniums</li>
<li>Sousaphones</li>
<li>Trombones</li>
<li>Tubas</li>
</ul>
<p>Art’s Music Shop provides in-store repair for the following <strong>woodwind instruments</strong>:</p>
<ul>
<li>Flutes</li>
<li>Piccolos</li>
<li>Clarinets</li>
<li>Saxophones</li>
<li>Oboes</li>
<li>English Horns</li>
<li>Bassoons</li>
</ul>
<p>Art’s Music Shop provides in-store repair for the following <strong>stringed instruments</strong>:</p>
<ul>
<li>Violins</li>
<li>Violas</li>
<li>Cellos</li>
<li>Double Bass</li>
<li>Mandolins</li>
<li>Guitars</li>
<li>Dobros</li>
<li>Pedal Steel Guitars</li>
<li>And more!</li>
</ul>
<p>Art’s Music Shop provides through-store repair for the following <strong>electronic equipment</strong>:</p>
<ul>
<li>Guitar Amplifiers</li>
<li>Bass Amplifiers</li>
<li>Mixing Boards</li>
<li>Other Select Pro Audio Equipment</li>
</ul>
</asp:Content>


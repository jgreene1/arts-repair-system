﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateRepair.aspx.cs" Inherits="WebApplication1.CreateRepair" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %> Create a new Repair Ticket  </h2>
            
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="TicketID" DataSourceID="SqlDataSource1">
        <InsertItemTemplate>
            CustomerID:
            <asp:TextBox BackColor="SlateGray" ID="CustomerIDTextBox" runat="server" Text='<%# Bind("CustomerID") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="CustomerIDTextBox"
                    CssClass="text-danger" ErrorMessage="The customer ID field is required." />
            <asp:RegularExpressionValidator runat="server" ControlToValidate="CustomerIDTextBox"
                    CssClass="text-danger" ErrorMessage="The Customer ID should be a number" ValidationExpression="^\d+$"/>
            <br />
            <br />
            RepID:
            <asp:TextBox BackColor="SlateGray" ID="RepIDTextBox" runat="server" Text='<%# Bind("RepID") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="RepIDTextBox"
                    CssClass="text-danger" ErrorMessage="The Rep ID field is required."  />
             <asp:RegularExpressionValidator runat="server" ControlToValidate="RepIDTextBox"
                    CssClass="text-danger" ErrorMessage="The Rep ID should be a number" ValidationExpression="^\d+$"/>
            <br />
            <br />          
            TicketNumber:
            <asp:TextBox BackColor="SlateGray" ID="TicketNumberTextBox" runat="server" Text='<%# Bind("TicketNumber") %>' />
             <asp:RequiredFieldValidator runat="server" ControlToValidate="TicketNumberTextBox"
                    CssClass="text-danger" ErrorMessage="The TickectNumber field is required." />
              <asp:RegularExpressionValidator runat="server" ControlToValidate="TicketNumberTextBox"
                    CssClass="text-danger" ErrorMessage="The Tickt Number should be a number" ValidationExpression="^\d+$"/>
            <br />
            <br />           
            Make:
            <asp:TextBox BackColor="SlateGray" ID="MakeTextBox" runat="server" Text='<%# Bind("Make") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="MakeTextBox"
                    CssClass="text-danger" ErrorMessage="The Make field is required." />
            <br />
            <br />
            Type:
            <asp:TextBox BackColor="SlateGray" ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>' />  
            <asp:RequiredFieldValidator runat="server" ControlToValidate="TypeTextBox"
                    CssClass="text-danger" ErrorMessage="The Type field is required." />        
            <br />
            <br />
            Model:
            <asp:TextBox BackColor="SlateGray" ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />           
            <br />
            <br />
            Serial:
            <asp:TextBox BackColor="SlateGray" ID="SerialTextBox" runat="server" Text='<%# Bind("Serial") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="TypeTextBox"
                    CssClass="text-danger" ErrorMessage="The Type field is required." />
            <asp:RegularExpressionValidator runat="server" ControlToValidate="SerialTextBox"
                    CssClass="text-danger" ErrorMessage="The Serial should be a number" ValidationExpression="^\d+$"/>
            <br />
            <br />
            DatePromised:
            <asp:Calendar BackColor="SlateGray" ID="DatePromisedCalendar" runat="server" SelectionMode="Day"
           SelectedDate='<%# Bind("DatePromised") %>' />          
            <br />
            <br />
            Accessories:
            <asp:TextBox BackColor="SlateGray" ID="AccessoriesTextBox" runat="server" Text='<%# Bind("Accessories") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="TypeTextBox"
                    CssClass="text-danger" ErrorMessage="The Type field is required." />
            <asp:RegularExpressionValidator runat="server" ControlToValidate="AccessoriesTextBox"
                    CssClass="text-danger" ErrorMessage="Accessories should be a number" ValidationExpression="^\d+$"/>
            <br />
            <br />
            Problem:
            <asp:TextBox BackColor="SlateGray" ID="ProblemTextBox" runat="server" Text='<%# Bind("Problem") %>' />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="TypeTextBox"
                    CssClass="text-danger" ErrorMessage="The Type field is required." />
            <br />
            <br />
            Description:
            <asp:TextBox BackColor="SlateGray" ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />            
            <br />
            <br />
            Shop:
            <asp:DropDownList ID="DropDownList1" runat="server" BackColor="SlateGray" Width="150px" SelectedValue='<%# Bind("Shop") %>' >
                    <asp:ListItem >Montgomery</asp:ListItem>
                    <asp:ListItem >Dothan</asp:ListItem>
                    <asp:ListItem >Birmingham</asp:ListItem>
                </asp:DropDownList>
            <br />
            <br />          
            <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
            &nbsp;<asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            TicketID:
            <asp:Label ID="TicketIDLabel" runat="server" Text='<%# Eval("TicketID") %>' />
            <br />
            CustomerID:
            <asp:Label ID="CustomerIDLabel" runat="server" Text='<%# Bind("CustomerID") %>' />
            <br />
            RepID:
            <asp:Label ID="RepIDLabel" runat="server" Text='<%# Bind("RepID") %>' />
            <br />          
            TicketNumber:
            <asp:Label ID="TicketNumberLabel" runat="server" Text='<%# Bind("TicketNumber") %>' />
            <br />
            DateCreated:
            <asp:Label ID="DateCreatedLabel" runat="server" Text='<%# Bind("DateCreated") %>' />
            <br />
            Make:
            <asp:Label ID="MakeLabel" runat="server" Text='<%# Bind("Make") %>' />
            <br />
            Type:
            <asp:Label ID="TypeLabel" runat="server" Text='<%# Bind("Type") %>' />
            <br />
            Model:
            <asp:Label ID="ModelLabel" runat="server" Text='<%# Bind("Model") %>' />
            <br />
            Serial:
            <asp:Label ID="SerialLabel" runat="server" Text='<%# Bind("Serial") %>' />
            <br />
            DatePromised:
            <asp:Label ID="DatePromisedLabel" runat="server" Text='<%# Bind("DatePromised") %>' />
            <br />
            Accessories:
            <asp:Label ID="AccessoriesLabel" runat="server" Text='<%# Bind("Accessories") %>' />
            <br />
            Problem:
            <asp:Label ID="ProblemLabel" runat="server" Text='<%# Bind("Problem") %>' />
            <br />
            Description:
            <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>' />
            <br />
            Shop:
            <asp:Label ID="ShopLabel" runat="server" Text='<%# Bind("Shop") %>' />
            <br />

            <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:artsRepairsConnectionString %>" SelectCommand="SELECT Repairs.* FROM Repairs" InsertCommand= "INSERT INTO Repairs(CustomerID, RepID, TicketNumber, DateCreated, Make, [Type], Model, DatePromised, Serial, Accessories, Problem, Shop, [Description]) VALUES (@CustomerID, @RepID, @TicketNumber, GETDATE(), @Make, @Type, @Model, @DatePromised, @Serial, @Accessories, @Problem, @Shop, @Description)" >
    </asp:SqlDataSource>

</asp:Content>

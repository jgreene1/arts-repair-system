﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class ViewOpenRepairs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //gets the user name and sets the default value for the UserName paramter on the select command
            SqlDataSource1.SelectParameters["UserName"].DefaultValue = User.Identity.Name;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //gets the user name and sets the default value for the UserName paramter on the select command
            SqlDataSource1.SelectParameters["UserName"].DefaultValue = User.Identity.Name;
        }
    }
}